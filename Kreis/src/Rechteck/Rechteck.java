package Rechteck;

public class Rechteck {

	//Attribute
	public double seiteA;
	public double seiteB;
	
	//Attribute
	//////////
	//Methode
	
	public Rechteck(double a, double b) {
		setSeiteA(a);
		setSeiteB(b);		
	}
	
	public Rechteck() {
		
	}
	
	public void diagonale() {
		ausgabe("Diagonale", Math.sqrt(getSeiteA()*getSeiteA() + getSeiteB()*getSeiteB()));
	}
	
	public void fleacheninhalt() {
		ausgabe("Flächeninhalt", getSeiteA()*getSeiteB());
	}
	
	public void umfang() {
		ausgabe("Umfang", getSeiteA()*2 + getSeiteB()*2);
	}
	
	public void verhaeltnis() {
		System.out.println("Verhältnis: " + "10:" + Math.round(10*getSeiteB()/getSeiteA()));
	}
	
	public static void ausgabe(String was, double ergebnis) {
		System.out.println(was + ": " + ergebnis);
	}
		
	//Methoden
	/////////////////
	//Geter und Setter
	public double getSeiteA() {
		return seiteA;
	}
	public void setSeiteA(double seiteA) {
		this.seiteA = seiteA;
	}
	public double getSeiteB() {
		return seiteB;
	}
	public void setSeiteB(double seiteB) {
		this.seiteB = seiteB;
	}
	
	
	
}
