import java.util.ArrayList;
import java.util.Random;

public class Raumschiff {

	// Attribute
	
	 /**@param name gibt den namen des Raumsschiffobjekts an.*/
	private String name;
	 /** @param energieversorgungInProzent gibt die Energieversorgung des Raumschiffsobjekts in Prozent an.*/
	private int energieversorgungInProzent;
	
	 /** @param schutzschildInProzent gibt den Schutzschild des Raumschiffsobjekts in Prozent an.*/
	private int schutzschildInProzent;
	
	 /** @param lebenserhaltungInProzent gibt die Lebenserhaltung des Raumschiffsobjekts in Prozent an.*/
	private int lebenserhaltungInProzent;
	
	 /** @param huelleInProzent gibt die Huelle des Raumschiffsobjekts in Prozent an.*/
	private int huelleInProzent;
	
	 /** @param photonentorpedosAnzahl gibt die ganzzahlige Anzahl der Abfeuerbaren Torpedos des Raumschiffobjekts wieder.*/
	private int photonentorpedosAnzahl;
	
	 /** @param reperaturdroidenAnzahl gibt die ganzzahlige Anzahl der einsetzbaren reperaturdroiden des Raumschiffobjekts wieder.*/
	private int reperaturdroidenAnzahl;
	
	 /** @param ladungsverzeichnis gibt eine Liste aller Ladungsobjekte in Form von Strings wieder.*/
	private ArrayList<String> ladungsverzeichnis = new ArrayList<String>();
	
	 /** @param broadcastKommunikator gibt den Verlauf der Ereignisse und Nachrichten in Form einer Liste von Strings wieder, den die Raumschiffobjtekte ausgetauscht haben*/
	private static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	
	 /** @param lobguch keine Definition.*/
	private static ArrayList<String> logbuch = new ArrayList<String>();
	
	// Attribute
	///////////////////////////////////
	// Constructor

	/**Constructor*/
	public Raumschiff() {

	}

	/**vollparametisierter Constructor*/
	public Raumschiff(String name, int energieversorgungInProzent, int schutzschildInProzent,
			int lebenserhaltungInProzent, int huelleInProzent, int photonentorpedosAnzahl, int reperaturdroidenAnzahl,
			ArrayList<String> ladungsverzeichnis) {

		setName(name);
		setEnergieversorgungInProzent(energieversorgungInProzent);
		setSchutzschildInProzent(schutzschildInProzent);
		setLebenserhaltungInProzent(lebenserhaltungInProzent);
		setHuelleInProzent(huelleInProzent);
		setPhotonentorpedosAnzahl(photonentorpedosAnzahl);
		setReperaturdroidenAnzahl(reperaturdroidenAnzahl);
		setLadungsverzeichnis(ladungsverzeichnis);
	}

	// Construktor
	////////////
	// Geter und Setter

	/**Geter f�r Attribut
	 * @return gibt den Namen des Raumschiffobjekts zur�ck.*/
	public String getName() {
		return this.name;
	}

	/**Seter f�r Attribut.
	 * @param name setzen des Namens des Raumschiffobjekts als String.*/
	public void setName(String name) {
		this.name = name;
	}

	/**Geter f�r Attribut.
	 * @return gibt die Energieversorgung des Raumschiffobjekts als Int zur�ck.*/
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	
	/**Seter f�r Attribut.
	 * @param energieversorgung setzen der Energieversorgung des Raumschiffsobjekts als Int.*/
	public void setEnergieversorgungInProzent(int energieversorgung) {
		this.energieversorgungInProzent = energieversorgung;
	}

	/**Geter f�r Attribut
	 * @return gibt den Schutzschild des Raumschiffobjekts als Int zur�ck.*/
	public int getSchutzschildInProzent() {
		return this.schutzschildInProzent;
	}

	/**Seter f�r Attribut.
	 * @param schutzschild setzen des Schutzschilds des Raumschiffsobjekts als Int.*/
	public void setSchutzschildInProzent(int schutzschild) {
		this.schutzschildInProzent = schutzschild;
	}

	/**Geter f�r Attribut
	 * @return gibt die Lebenserhaltung des Raumschiffobjekts als Int zur�ck.*/
	public int getLebenserhaltungInProzent() {
		return this.lebenserhaltungInProzent;
	}

	/**Seter f�r Attribut.
	 * @param lebenserhaltung setzen der Lebenserhaltung des Raumschiffsobjekts als Int.*/
	public void setLebenserhaltungInProzent(int lebenserhaltung) {
		this.lebenserhaltungInProzent = lebenserhaltung;
	}

	/**Geter f�r Attribut
	 * @return gibt die H�lle des Raumschiffobjekts als Int zur�ck.*/
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}

	/**Seter f�r Attribut.
	 * @param setzen der H�lle des Raumschiffsobjekts als Int.*/
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}

	/**Geter f�r Attribut
	 * @return gibt die Anzahl der feuerbereiten Torpedos des Raumschiffobjekts als Int zur�ck.*/
	public int getPhotonentorpedosAnzahl() {
		return this.photonentorpedosAnzahl;
	}

	/**Seter f�r Attribut.
	 * @param photonentorpedos setzen der Anzahl von feuerbereiten Photonentorpedos des Raumschiffsobjekts als Int.*/
	public void setPhotonentorpedosAnzahl(int photonentorpedos) {
		this.photonentorpedosAnzahl = photonentorpedos;
	}

	/**Geter f�r Attribut
	 * @return gibt die Anzahl an einsatzbereiten reperatur Droiden des Raumschiffobjekts als Int zur�ck.*/
	public int getReperaturdroidenAnzahl() {
		return this.reperaturdroidenAnzahl;
	}

	/**Seter f�r Attribut.
	 * @param reperaturdroiden setzen der Anzahl von einsatzbereiten Reperaturdroiden des Raumschiffsobjekts als Int.*/
	public void setReperaturdroidenAnzahl(int reperaturdroiden) {
		this.reperaturdroidenAnzahl = reperaturdroiden;
	}

	/**Geter f�r Attribut
	 * @return gibt das Ladungsverzeochnis des Raumschiffobjekts als String ArrayList zur�ck.*/
	public ArrayList<String> getLadungsverzeichnis() {
		return this.ladungsverzeichnis;
	}

	/**Seter f�r Attribut.
	 * @param ladungsverzeichnis setzen des Ladungsverzeichnises des Raumschiffsobjekts als String ArrayList.*/
	public void setLadungsverzeichnis(ArrayList<String> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}
	
	/**Geter f�r Attribut
	 * @return gibt den Broadcast des Raumschiffobjekts als String ArrayList zur�ck.*/
	public static ArrayList<String> getBroadcastKommunikator() {
		return broadcastKommunikator;
	}
	
	/**Seter f�r Attribut.
	 * @param broadcastKommunikator setzen des Broadcasts des Raumschiffsobjekts als String ArrayList.*/
	public static void setBroadcastKommunikator(ArrayList<String> broadcastKommunikator) {
		Raumschiff.broadcastKommunikator = broadcastKommunikator;
	}
	
	/**Geter f�r Attribut
	 * @return gibt das Loguch des Raumschiffobjekts als String ArrayList zur�ck.*/
	public static ArrayList<String> getLogbuch() {
		return logbuch;
	}
	
	/**Seter f�r Attribut.
	 * @param logbuch setzen des Logbuchs des Raumschiffsobjekts als String ArrayList.*/
	public static void setLogbuch(ArrayList<String> logbuch) {
		Raumschiff.logbuch = logbuch;
	}

	// Geter und Seter
	/////////////////
	// Methoden

	/** Feuert die Photonentorpedos auf ein Ziel.
	 *  Kann nur feuern wenn Photonentorpedos geladen und Feuerbereit sind.
	 *  @param ziel Es wird das Raumschiffobjekt, welches von einem Photonentorpedo getroffen wird, �bergeben.*/
	public void photonentorpedosFeuern(Raumschiff ziel) {
		if (getPhotonentorpedosAnzahl() == 0) {
			broadcast(this.getName() + ": -=*Click*=-");
		} else {
			setPhotonentorpedosAnzahl(getPhotonentorpedosAnzahl() - 1);
			broadcast(this.getName() + ": Photonentorpedos abgeschossen");
			hitMarker(ziel);
		}
	}

	/** Feuert die Energieabh�ngigen Phaserkanonen auf ein Ziel.
	 *  Kann nur Feuern wenn die Energieversorgung �ber 50% liegt.
	 *  @param ziel Es wird das Raumschiffobjekt, welches von einer Phaserkanonen getroffen wird, �bergeben .*/
	public void phaserkanonenFeuern(Raumschiff ziel) {
		if (getEnergieversorgungInProzent() < 50) {
			broadcast(this.getName() + ": -=*Click*=-");
		} else {
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() - 50);
			broadcast(this.getName() + ": Phaserkanone abgeschossen");
			hitMarker(ziel);
		}
	}

	/** Vermerkt das ein Schiff getroffen wurde und berechnet angerichteten
	 *  schaden und in welchem zustand das getroffene Schiff verbleibt
	 *  @param ziel Das von einer waffe getroffene Raumschoffobjekt wird �bergeben*/
	private void hitMarker(Raumschiff ziel) {
		System.out.print(ziel.getName() + " wurde getroffen!\n");

		// Zun�chst muss der Schild auf 0% gebracht werden - jeder treffer zieht 50%
		// Schilde ab
		// Ist der Schild auf 0 so wird bei jedem treffer 50% von der Energieversorung
		// und H�lle abgezogen
		// Falls dabei die H�lle auf 0% Reduziert wird, ist die Lebenserhaltung zerst�rt
		if (ziel.getSchutzschildInProzent() > 0) {
			ziel.setSchutzschildInProzent(ziel.getSchutzschildInProzent() - 50);
		} else {
			ziel.setEnergieversorgungInProzent(ziel.getEnergieversorgungInProzent() - 50);
			ziel.setHuelleInProzent(ziel.getHuelleInProzent() - 50);
			if (ziel.getHuelleInProzent() <= 0) {
				ziel.setLebenserhaltungInProzent(0);
				broadcast("Die Lebenserhaltungssysteme von " + ziel.getName() + " sind zerst�rt!");
			}
		}
	}

	/** Speichern der Nachrichten im BroadcastKommunikator und der Ausgabe in der Konsole.
	 *  @param nachrichten Eine Nachricht als String zum speichern und ausgeben wird �bergeben.*/
	public void broadcast(String nachrichten) {
		broadcastKommunikator.add(nachrichten);

		System.out.print(nachrichten + "\n" );
	}

	/** Ladung wird im Ladungsverzeichnis des Raumschiffs vermerkt und dort verwaltet
	 *  @param ladung Es wird ein Ladungsobjekt �bergeben das auf ein Raumschiff geladen werden soll.*/
	public void ladungEingehend(Ladung ladung) {
		ArrayList<String> ladungsver = getLadungsverzeichnis();

		ladungsver.add(ladung.getBezeichner() + " x" + ladung.getAnzahl());
		setLadungsverzeichnis(ladungsver);
	}

	/**Es soll eine Ganzzahl von Photonentorpedos feuerbereit geladen werden.
	 * Diese Anzahl wird mit den geladenen Torpedos im Ladungsverzeichnis abgegeglichen.
	 * Ist die Anzahl zu der zum feuern bereit zu machenden Torpedos gr��er als die Anzahl geladenener Photonentorpedos, so werden alle geladenen Torpedos feuerbereit gemacht.
	 * Sind keine Torpedos geladen so wird eine MEldung mit "Click" ausgegeben und kein Torpedo feuerbereit gemacht. 
	 * @param ladenAnz Es wird eine ganzzahlige Anzahl als Int von Photonentorpedos �bergeben, die auf einem Raumschiff geladen werden soll.*/
	public void photonentorpedosLaden(int ladenAnz) {
		String[] split = null;
		int photoIndex = -1;
		int photoAnz;
		ArrayList<String> ladungsver = getLadungsverzeichnis();

		// im ladungsverzeichnis nach den torpedos suchen
		for (int i = 0; i < getLadungsverzeichnis().size(); i++) {
			split = getLadungsverzeichnis().get(i).split(" x");
			if (split[0].equals("Photonentorpedos")) {
				photoIndex = i;
				break;
			}
		}

		// ist der Index negativ, so ist keine torpedoladung vorhanden
		if (photoIndex >= 0) {

			// entnehmen und parsen der Ladungsanzahl
			photoAnz = Integer.parseInt(split[1]);

			// photoAnz vergleich mit der angeforderten menge
			if (ladenAnz > photoAnz) {
				setPhotonentorpedosAnzahl(getPhotonentorpedosAnzahl() + photoAnz);
				ladungsver.set(photoIndex, "Photonentorpedos x0");
				setLadungsverzeichnis(ladungsver);
				System.out.print("\n" + photoAnz + " Photonentorpedos eingesetzt");

				ladungsverClean();
			} else {
				setPhotonentorpedosAnzahl(getPhotonentorpedosAnzahl() + ladenAnz);
				ladungsver.set(photoIndex, "Photonentorpedos x" + (photoAnz - ladenAnz));
				setLadungsverzeichnis(ladungsver);
				System.out.print("\n" + ladenAnz + " Photonentorpedos eingesetzt");

				ladungsverClean();
			}
		} else {
			System.out.print("\nKeine Photonentorpedos gefunden!");
			broadcast(this.getName() + ": -=*Click*=-");
		}
	}

	/** Das Reparieren der schildeInProzent, energieversorgungInProzent, huelleInProzent durch eine Anzahl von Reperaturdroiden.
	 *  Welche Bereiche Repariert werden sollen wird durch �bergebene booleans dargestellt. 
	 *  Eine zuff�llige Zahl zwischen 1 & 100 wird zu den angegebenen Bereichen Addiert.
	 *  @param schild true = reparieren der SchildeInProzent des Raumschiffobjekts. false = keine reperatur.
	 *  @param energieversorgung true = reparieren der energieversorgungInProzent des Raumschiffobjekts. false = keine reperatur.
	 *  @param huelle true = reparieren der huelleInProzent des Raumschiffobjekts. false = keine reperatur.
	 *  @param droidenAnz Ganzzahl zu nutzenden Reperatur droiden*/
	public void reperatur(Boolean schild, Boolean energieversorgung, Boolean huelle, int droidenAnz) {

		Random wuerfel = new Random();
		int dice = wuerfel.nextInt(101);
		int strukturen = 0;
		int repariertUm;

		// Wenn die zu nutzende Droiden Anzahl mehr ist als das Schiff geladen hat, so
		// werden alle genutzt
		if (getReperaturdroidenAnzahl() < droidenAnz) {
			droidenAnz = getReperaturdroidenAnzahl();
		}

		// Berechnung wieviel insgesamt Repariert wird
		strukturen += (schild == true) ? 1 : 0;
		strukturen += (energieversorgung == true) ? 1 : 0;
		strukturen += (huelle == true) ? 1 : 0;

		repariertUm = (int) Math.floor((dice * droidenAnz) / strukturen);

		// Reparieren der angegebenen Bereiche
		if (schild == true) {
			setSchutzschildInProzent(getSchutzschildInProzent() + repariertUm);
		}
		if (energieversorgung == true) {
			setEnergieversorgungInProzent(getEnergieversorgungInProzent() + repariertUm);
		}
		if (huelle == true) {
			setHuelleInProzent(getHuelleInProzent() + repariertUm);
		}
	}

	/** Ausgabe aller Attribute des Raumschiffs in der Konsole.*/
	public void schadensbericht() {
		System.out.printf(
				"\n//////Schadensbericht\\\\\\\\\\\\ \n" + "%-24s" + getName() + "\n" + "%-24s"
						+ getEnergieversorgungInProzent() + "\n" + "%-24s" + getSchutzschildInProzent() + "\n" + "%-24s"
						+ getLebenserhaltungInProzent() + "\n" + "%-24s" + getHuelleInProzent() + "\n" + "%-24s"
						+ getPhotonentorpedosAnzahl() + "\n" + "%-24s" + getReperaturdroidenAnzahl() + "\n" + "%-24s"
						+ getLadungsverzeichnis() + "\n" + "------------------------------\n",
				"Name:", "Energieversorgung:", "Schutzschild:", "Lebenserhaltung:", "H�lle:", "Photonentorpedos:",
				"Reperaturdroiden:", "Ladungsverzeichnis:");
	}

	/** Gibt das Ladungsverzeichnis eines Raumschiffs in der Konsole aus. */
	public void ladungsverKonsoleOut() {
		ArrayList<String> ladungsver = getLadungsverzeichnis();

		System.out.print("\n+++LADUNGSVERZEICHNIS+++\n");
		for (int i = 0; i < ladungsver.size(); i++) {
			System.out.print(ladungsver.get(i) + "\n");
		}
		System.out.print("------------------------\n");
	}

	/** Ist das Ladungsverzeichnis mit einer leeren Ladung bef�llt, so wird diese aus dem Ladungsverzeichnis entfernt. */
	public void ladungsverClean() {
		String[] split;
		ArrayList<String> ladungsver = getLadungsverzeichnis();

		for (int i = 0; i < ladungsver.size(); i++) {
			split = ladungsver.get(i).split(" x");
			if (split[1].equals("0")) {
				ladungsver.remove(i);
				setLadungsverzeichnis(ladungsver);

				ladungsverClean();
				break;
			}
		}
	}

	/** Ausgabe des logbuchs und des Broadcast Kommunikators in der Konsole. */
	public static void logbuchOut() {
		// Ausgabe des Logbuchs
		System.out.print("\n\n+-----------Logbuch-----------+\n");
		for (int i = 0; i < logbuch.size(); i++) {
			System.out.println(logbuch.get(i) + "\n");
		}
		System.out.print("+-----------------------------+\n");

		// Ausgabe der Broadcast Kommunikators
		System.out.print("\n\n+---+BroadcastKommunikator+---+\n");
		for (int i = 0; i < broadcastKommunikator.size(); i++) {
			System.out.println(broadcastKommunikator.get(i) + "\n");
		}
		System.out.print("+-----------------------------+\n");
	}

}
