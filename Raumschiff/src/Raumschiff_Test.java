import java.util.ArrayList;

public class Raumschiff_Test {

	public static void main(String[] args) {

		/////////////////////////////
		// Raumschiffe Initialisieren
		/*
		 * public Raumschiff( String name, int energieversorgung, int schutzschild, int
		 * lebenserhaltung, int huelle, int photonentorpedos, int reperaturdroiden,
		 * ArrayList<String> ladungsverzeichnis)
		 */

		Raumschiff klingonen = new Raumschiff("IKS Hegh'ta", 100, 100, 100, 100, 1, 2, new ArrayList<String>());

		Raumschiff romulaner = new Raumschiff("IRW Khazara", 100, 100, 100, 100, 2, 2, new ArrayList<String>());

		Raumschiff vulkanier = new Raumschiff("Ni'var", 80, 80, 100, 50, 0, 5, new ArrayList<String>());

		/////////////////////////
		// Ladungen Initialisieren

		Ladung ferengiS = new Ladung("Ferengi Schneckensaft", 200);
		Ladung borgS = new Ladung("Borg-Schrott", 5);
		Ladung roteM = new Ladung("Rote Materie", 2);
		Ladung forschungsS = new Ladung("Forschungssonde", 35);
		Ladung batKS = new Ladung("Bat'leth Klingonen Schwert", 200);
		Ladung plasmaW = new Ladung("Plasma-Waffe", 50);
		Ladung photonenT = new Ladung("Photonentorpedos", 3);

		//////////
		// Beladen

		klingonen.ladungEingehend(ferengiS);
		klingonen.ladungEingehend(batKS);
		romulaner.ladungEingehend(borgS);
		romulaner.ladungEingehend(roteM);
		romulaner.ladungEingehend(plasmaW);
		vulkanier.ladungEingehend(forschungsS);
		vulkanier.ladungEingehend(photonenT);

		//////////
		// Main

		klingonen.photonentorpedosFeuern(romulaner);

		romulaner.phaserkanonenFeuern(klingonen);

		vulkanier.broadcast("Gewalt ist nicht logisch");

		klingonen.schadensbericht();
		klingonen.ladungsverKonsoleOut();

		vulkanier.reperatur(true, true, true, vulkanier.getReperaturdroidenAnzahl());

		vulkanier.photonentorpedosLaden(photonenT.getAnzahl());
		vulkanier.ladungsverClean();

		klingonen.photonentorpedosFeuern(romulaner);
		klingonen.photonentorpedosFeuern(romulaner);

		klingonen.schadensbericht();
		klingonen.ladungsverKonsoleOut();
		romulaner.schadensbericht();
		romulaner.ladungsverKonsoleOut();
		vulkanier.schadensbericht();
		vulkanier.ladungsverKonsoleOut();
	}
}

/*
 * 
 * 
 * 
 * 
 * 
 * 
 * klingonen 1 torpedo
 * Romulaner und Klingonen alle prozente auf 100 
 * 
 *
 * klingonen -1 Torpedos -> romulaner schilde -50
 * romulaner energie -50 -> klingonen schilde -50
 * klingonen "click" (0 torpedos)
 * 
 * klingonen 0 torpedos schilde = 50
 * romulaner energie = 50 schilde = 50
 * */
