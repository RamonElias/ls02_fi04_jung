public class Ladung {

	// Attribute
	
	/**@param Bezeichnug der Ladung. */
	public String bezeichnung;
	
	/** @param Die anzahl der Ladung. */
	public int anzahl;
	
	// Attribute
	///////////////////////////////////
	//////////////////////////////////
	// Methoden

	///////////////////
	//// geter und seter

	/**geter des Attributs
	 * @return gibt die Bezeichnug der Ladung als String zur�ck.*/
	public String getBezeichner() {
		return this.bezeichnung;
	}

	/**seter des Attributs
	 * @param bezeichner die Bezeichnung des Ladungsobjekts als String.*/
	public void setBezeichner(String bezeichner) {
		this.bezeichnung = bezeichner;
	}

	/**geter des Attributs
	 * @return gibt die Anzahl der Ladung als Int zur�ck.*/
	public int getAnzahl() {
		return this.anzahl;
	}

	/**seter des Attributs
	 * @param anzahl die Anzahl der Bezeichneten Ladung des Ladungsobjekts als Int.*/
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}

	//// geter und seter
	///////////////////////
	//////////////////////
	//// Konstruktor

	/** Constructor */
	public Ladung() {

	}

	/** Vollparametisierter Constructor */
	public Ladung(String bezeichnung, int anzahl) {

		setBezeichner(bezeichnung);
		setAnzahl(anzahl);
	}

}
